"""wpcs URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from soil.views import soilmoisture_api, home, threshold_api, save_soilmoisture_value, sensor_valus_api

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^soilmoisture/api/$', soilmoisture_api, name='soilmoisture_api'),
    url(r'^sensor_valus/api/$', sensor_valus_api, name='sensor_valus_api'),
    url(r'^threshold/api/$', threshold_api, name='threshold_api'),
    url(r'^save/soilmoisture_value/(?P<moisture>.+)/api/$', save_soilmoisture_value, name='save_soilmoisture_value'),
    url(r'^$', home, name='home')
]
