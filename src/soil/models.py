from django.db import models
# Create your models here.

class SoilMoisture(models.Model):
    sensor1 = models.CharField(max_length=255)
    sensor2 = models.CharField(max_length=255)
    sensor3 = models.CharField(max_length=255)
    sensor4 = models.CharField(max_length=255)
    cluster1_avg = models.CharField(max_length=255)
    cluster2_avg = models.CharField(max_length=255)
    cluster1 = models.CharField(max_length=255)
    cluster2 = models.CharField(max_length=255)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def _str__(self):
        return str(self.id)

class ThresholdValue(models.Model):
    for_cluster1 = models.CharField(max_length=255)
    for_cluster2 = models.CharField(max_length=255)

    def _str__(self):
        return str(self.id)
