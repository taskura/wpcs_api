from django.shortcuts import render
from django.http import JsonResponse
from .models import SoilMoisture, ThresholdValue
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime, timedelta
# Create your views here.

def sensor_valus_api(request):
    data = dict()
    soilmoisture = SoilMoisture.objects.last()
    data["sensor1"] = soilmoisture.sensor1
    data["sensor2"] = soilmoisture.sensor2
    data["sensor3"] = soilmoisture.sensor3
    data["sensor4"] = soilmoisture.sensor4

    return JsonResponse(data)

 # data={{
 #     labels: ['5.00pm', '5.15pm','5.30pm'],
 #     legend: ['Have water', 'Need water'],
 #     data: [[90, 10], [80, 20], [60, 40]],
 #     barColors: ['#297d4e', '#fc0313'],
 #   }}

def soilmoisture_api(request):
    data = dict()
    soilmoisture = SoilMoisture.objects.all().order_by("-id")[:3]

    data_sample1 = []
    dict_sample1 = {
        "labels":[],
        "data":[],
    }

    data_sample2 = []
    dict_sample2 = {
        "labels":[],
        "data":[],
    }

    for moisture in soilmoisture:
        in_time = datetime.strptime(str(moisture.timestamp.time())[:8], "%H:%M:%S")
        out_time = datetime.strftime(in_time, "%I:%M %p")
        dict_sample1['labels'].append(out_time)
        dict_sample2['labels'].append(out_time)

        data_sample1.append(moisture.cluster1_avg)
        data_sample1.append(moisture.cluster1)
        dict_sample1['data'].append(data_sample1)

        data_sample2.append(moisture.cluster2_avg)
        data_sample2.append(moisture.cluster2)
        dict_sample2['data'].append(data_sample2)

        data_sample1 = []
        data_sample2 = []

    data['chart1'] = dict_sample1
    data['chart2'] = dict_sample2
    return JsonResponse(data)


@csrf_exempt
def save_soilmoisture_value(request, moisture):
    data = dict()
    print(moisture)
    try:
        soilmoisture = eval(moisture)
    except Exception as e:
        soilmoisture = None

    if soilmoisture:
        print("Valid Dict")
        # {"s1":2,"s2":2,"s3":5,"s4":6,"c1v":2,"c2v":5,"c1":78,"c2":75}
        SoilMoisture.objects.create(
            sensor1 = soilmoisture['s1'],
            sensor2 = soilmoisture['s2'],
            sensor3 = soilmoisture['s3'],
            sensor4 = soilmoisture['s4'],
            cluster1_avg = soilmoisture['c1v'],
            cluster2_avg = soilmoisture['c2v'],
            cluster1 = soilmoisture['c1'],
            cluster2 = soilmoisture['c2'],
        )

    return JsonResponse(data)

def threshold_api(request):
    data = dict()
    threshold = ThresholdValue.objects.last()
    data['t1'] = threshold.for_cluster1
    data['t2'] = threshold.for_cluster2
    data['s'] = "open"

    return JsonResponse(data)

@csrf_exempt
def update_threshold_value(request):
    data = dict()
    if request.method == "POST":
        print("Yooooooooooooooooooo")
    return JsonResponse(data)


def home(request):
    template_name = "home.html"
    context = {}
    return render(request, template_name, context)
