from django.contrib import admin
from .models import SoilMoisture, ThresholdValue
# Register your models here.

admin.site.register(SoilMoisture)
admin.site.register(ThresholdValue)
